local function sendResponse(connection, httpCode, data)
   connection:send("HTTP/1.0 "..httpCode.." OK\r\nContent-Type: application/json\r\nCache-Control: private, no-store\r\n\r\n")
   connection:send(data)
end

local function tojsonarray(data)
   s = ""
   for _, v in ipairs(data) do s = s..","..tostring(v) end
   return "[" .. s:sub(2) .. "]"
end

return function (connection, req, args)

   if req.method == "GET" then
      local message = "[]"
      if _G.temp then message = tojsonarray(_G.temp) end
      sendResponse(connection, 200, message)
      return
   end

   sendResponse(connection, 400, '"error"')

end
