local TEMP_PIN = 6
local RING_SIZE = 100
_G.temp = {}

ow.setup(TEMP_PIN)
local addr = ow.search(TEMP_PIN)

local function push(tbl, num, max)
   local new_table = {num}
   local size = math.min(#tbl, max-1)
   for i=1,size do
      new_table[i+1] = tbl[i]
   end
   return new_table
end

local function readT_bottom(pin, addr)
   ow.reset(pin)
   ow.select(pin, addr)
   ow.write(pin, 0xBE, 1)

   local data = ""
   for i = 1, 9 do
      data = data .. string.char(ow.read(pin))
   end

   local t
   if _G.convert_t then
      t = _G.convert_t(data)
   else
      local sign = 1
      if (bit.isset(data:byte(2), 7)) then
         sign = -1
      end
      t = sign * (bit.bor(bit.rshift(data:byte(1),4), bit.lshift(bit.band(data:byte(2), 0x7),4))
                  + bit.band(data:byte(1), 0xf) * 0.0625)
   end
   _G.temp = push(_G.temp, t, RING_SIZE)
end

local function readT_top(pin, addr)
   ow.reset(pin)
   ow.select(pin, addr)
   ow.write(pin, 0x44, 1)

   local convert_delay = tmr.create()
   convert_delay:register(750, tmr.ALARM_SINGLE, function (t) readT_bottom(pin, addr); t:unregister() end)
   convert_delay:start()
end

local mytimer = tmr.create()
mytimer:register(
   5000, tmr.ALARM_AUTO,
   function(_)
      readT_top(TEMP_PIN, addr)
   end
)
mytimer:start()
